<?php
    /**
    * FileMaker PHP Site Assistant Generated File
    */
    require_once 'fmview.php';
    require_once 'FileMaker.php';
    require_once 'error.php';

    $cgi = new CGI();
    $cgi->storeFile();
    
    $databaseName = 'ARSC Index';
    $layoutName = 'web';

    $fm = new FileMaker();
    $fm->setProperty('database', $databaseName);    
    ExitOnError($fm);
    $layout = $fm->getLayout($layoutName);
    ExitOnError($layout);

    // formats for dates and times
    $displayDateFormat = '%m/%d/%Y';
    $displayTimeFormat = '%I:%M:%S %P';
    $displayDateTimeFormat = '%m/%d/%Y %I:%M:%S %P';
    $submitDateOrder = 'mdy';
    $record = NULL;
    $findCom = NULL;
    $findAll = NULL;
    
    $value = $cgi->get('-sortfieldone');
    $restore = $cgi->get('-restore');
    if(!isset($value) || $restore == 'true'){
        $cgi->clear("-restore");
    }

    //  handle the action cgi
    $action = $cgi->get('-action');
    if ($action == "findall") {
        $cgi->clear('skip');
        $findAll = true;
    }
        
    // clear the recid
    $cgi->clear('recid');

    // create a find command
    $findCommand = $fm->newFindCommand($layoutName);
    ExitOnError($findCommand);

    // get the posted record data from the findrecords page
    $findrequestdata = $cgi->get('storedfindrequest');
    if (isset($findrequestdata)) {
       $findCom = prepareFindRequest($findrequestdata, $findCommand, $cgi);

		// set the logical operator
		$logicalOperator = $cgi->get('-lop');
		if (isset($logicalOperator)) {
			$findCom->setLogicalOperator($logicalOperator);
		}
	} else
       $findCom = $fm->newFindAllCommand($layoutName);
    
    ExitOnError($findCom);

    // read and set, or clear the sort criteria
    $sortfield = $cgi->get('-sortfieldone');
    if (isset($sortfield)) {
        addSortCriteria($findCom);
    } else {
        clearSortCriteria($findCom);
    }

    // get the skip and max values
    $skip = $cgi->get('-skip');
    if (isset($skip) === false) {
        $skip = 0;
    }
    $max = $cgi->get('-max');
    if (isset($max) === false) {
        $max = 25;
    }

    // set skip and max values
    $findCom->setRange($skip, $max);

    // perform the find
    $result = $findCom->execute();
    ExitOnError($result);
    
    // get status info; page range, found count, total count, first, prev, next, and last links
    $statusLinks = getStatusLinks("recordlist.php", $result, $skip, $max);

    // get the records
    $records = $result->getRecords(); 
    
    $storedLink = $cgi->get('-link');
	if ( !is_null($storedLink) && $storedLink == 'all')
		$title = 'Show all Records';
	else
		$title = 'Search Results';
?><!DOCTYPE HTML>
<!-- InstanceBegin template="/Templates/subpage.dwt" codeOutsideHTMLIsLocked="true" -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-3LC513DQ73"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-3LC513DQ73');
</script>

<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0, user-scalable=yes" />

<link rel="stylesheet" type="text/css" href="http://www.arsc-audio.org/arsc.css" />
<link rel="stylesheet" type="text/css" media="screen" href="arsc-journal-index.css" />

<!-- InstanceBeginEditable name="doctitle" -->
<title>ARSC Journal Index(Association for Recorded Sound Collections)</title>
<!-- InstanceEndEditable -->    
<link rel="shortcut icon" href="/images/favicon.ico" type="image/vnd.microsoft.icon"> 
<link rel="icon" href="/images/favicon.ico" type="image/vnd.microsoft.icon"> 
<!-- InstanceBeginEditable name="head" -->
<meta name="description" content="The ARSC Journal is a bi-annual, peer reviewed publication that serves to document the history of sound recording and includes original articles on many aspects of research and preservation: biography; cataloging; copyright law; current research; discography; technical aspects of sound restoration, etc., etc. Selected ARSC conference papers are a regular feature. The journal also includes book, CD-ROM and sound recording reviews, and publishes a running bibliography of articles appearing in other specialist publications and of related interest. (Association for Recorded Sound Collections).  ">
<meta name="keywords" content="arsc journal bi-annual peer reveied publication arsc association recorded sound collections">
<!-- InstanceEndEditable -->
</head>
<body>
<div id="skip-links" title="Links that allow you to skip to certain parts of the page">
		    <a href="#main-menu">Skip to navigation</a>	
			<a href="#content">Skip to content</a>
			<a href="/contact.html">Contact us about accessibility issues</a>
  		</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#E5E5E5"> 
    <td width="56%"><a name="top"></a><img src="images/arsc.gif" alt="Association for Recorded Sound Collections (ARSC)" width="574" height="73"></td>
    <td width="44%" bgcolor="#E5E5E5"> 
            
<form action="http://www.arsc-audio.org/search-results.html" id="cse-search-box">
<div id="banner-menu"> 
    <input type="hidden" name="cx" value="005131583914068578997:kwfltgaqcgg" />
    <input type="hidden" name="cof" value="FORID:9" />
    <input type="hidden" name="ie" value="UTF-8" />
    <input type="text" name="q" size="20" />
    <input type="submit" name="sa" value="Search" />
</form>
<script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&lang=en"></script>

    <br>
    <a href="/about.html">about</a> | <a href="/join.html">join or renew</a> 
           | <a href="/members-only-login.html">member login</a> | <a href="/contact.html">contact</a> | <!-- Facebook Badge START --><a href="http://www.facebook.com/pages/Association-for-Recorded-Sound-Collections-ARSC/165687321898"><img src="/images/f_logo.png" width="20" height="20" style="border: 0px;" /></a><!-- Facebook Badge END --> <!-- LinkedIn Badge START --> <a href="https://www.linkedin.com/groups?home=&gid=3967774&trk=groups_statistics-h-logo&goback=%2Egmr_3967774"><img src="http://www.arsc-audio.org/images/linkedin_logo.png" width="20" height="20" style="border: 0px;" /></a><!-- LinkedIn Badge END --><br/>
      </td>
  </tr>
  <tr> 
    <td bgcolor="#000000"><div id="crumbs"><a href="/index.html">home</a>
<!-- InstanceBeginEditable name="top-crumb" --> 
        &raquo; <a href="/publications.html">publications</a> 
        &raquo; <a href="/journal.html">arsc journal</a> 
        &raquo; <strong>index</strong>
        <!-- InstanceEndEditable -->
        </div>
      <div id="pubs"><a href="/publications.html">publications</a>: <a href="/newsletter/index.html">newsletter</a>&nbsp;</div></td>
    <td bgcolor="#000000"><div id="pubs2">| <a href="/journal.html">journal</a> | <a href="/cds.html">conference recordings</a></div></td>
  </tr>
  <tr> 
    <td colspan="2"><div id="subpage-content">
        <!-- InstanceBeginEditable name="page-content" --> 
		<!-- Content -->
		<div id="content">
			<h1>ARSC Journal Index: <?php print $title; ?></h1>
			<ul id="local-links">
<?php 
$storedLink = $cgi->get('-link');
if (is_null($storedLink)) $cgi->store('-link', 'home'); 

if (isset($cgi)) {
	$activelink = $cgi->get('-link');
	$cgi->clear('-link');
}

$localURLs =  array('find' => '<a href="findrecords.php?-link=find">Find Records</a>',
								'all' => '<a href="recordlist.php?-max=25&amp;-skip=0&amp;-link=all&amp;-action=findall&amp;-sortfieldone=Volume&amp;-sortorderone=ascend">
                                Show all Records</a>',
										);
									  
foreach ($localURLs as $name => $link) {
	if (isset($activelink) && (strcmp($name, $activelink) === 0)) {
		echo "<li class='activelink'><img height='10' width='11' alt='Find Records' src='./images/arrow-right.gif'>$link</li>";
		unset($activelink);
	} else {
		echo "<li><img height='10' width='11' alt='Find Records' src='./images/arrow-right.gif'>$link</li>";
	}
}
?>
</ul>

			<!--  Display record list page navigation controls -->
			<ul id="recordlist_nav">
				<li class="recordlist_nav_first">« <?php echo $statusLinks['first']; ?></li>
				<li>|</li>
				<li class="recordlist_nav_prev">‹ <?php echo $statusLinks['prev']; ?></li>
				<li>|</li>
				<li class="recordlist_nav_range">Record <?php echo $statusLinks['records']['rangestart']; ?> - <?php echo $statusLinks['records']['rangeend']; ?> of <?php echo $statusLinks['records']['foundcount']; ?></li>
				<li>|</li>
				<li class="recordlist_nav_next"><?php echo $statusLinks['next']; ?>  ›</li>
				<li>|</li>
				<li class="recordlist_nav_last"><?php echo $statusLinks['last']; ?> »</li>
			</ul>

			<table class="browse_records">
				<thead>
					<tr>
						<th class="browse_header left"><?php echo getSortRecordsLink('Author', 'Author') //('field_name','display name')?></th>
						<th class="browse_header left"><?php echo getSortRecordsLink('Title', 'Title')?></th>
						<th class="browse_header left"><?php echo getSortRecordsLink('Resource_Type', 'Resource Type')?></th>
						<th class="browse_header left"><?php echo getSortRecordsLink('Filename', 'Access')?></th>
						<th class="browse_header left"><?php echo getSortRecordsLink('Volume', 'Volume')?></th>
						<th class="browse_header"><?php echo getSortRecordsLink('Pages', 'Pages')?></th>
					</tr>
				</thead>
				<tbody>
					<?php
						$recnum = 1;
						foreach ($records as $fmrecord):
							$record = new RecordHighlighter($fmrecord, $cgi);

							$rowclass = ($recnum % 2 == 0) ? "table_row" : "alt_row";
							$recid = $record->getRecordId();
							$pos = strpos($recid, "RID_!");
							if ($pos !== false) {
								 $recid = substr($recid,0,5) . urlencode(substr($recid,strlen("RID_!")));
						}
					?>
					<tr class="<?php echo $rowclass ?>">
						<td class="browse_cell left"><?php echo nl2br($record->getField('Author', 0)) ?></td>
						<td class="browse_cell left"><?php echo nl2br($record->getField('Title', 0))  ?></td>
						<td class="browse_cell left"><?php echo nl2br($record->getField('Resource_Type', 0)) ?></td>
						<td class="browse_cell left"><!-- Access column -->
                            <a href="<?php echo $record->getField('URLShort', 0); ?>" target="_blank">Download</a>
						</td>					
						<td class="browse_cell left"><?php echo nl2br(str_replace(' ', '&nbsp; ', $record->getField('Volume', 0) ))?></td>
						<td class="browse_cell"><?php echo nl2br(str_replace(' ', '&nbsp; ', $record->getField('Pages', 0) ))?></td>
					</tr>
					<?php 
						$recnum++;
						endforeach;
					?>
				</tbody>
			</table>            
        </div>
        <!-- End of Content -->	
		
		<!-- Footer-->
		<div id="footer">
			<div>
				<div id="footer-left">
				</div>
		<!-- End of Content -->
<p>&nbsp;</p>
<p><img src="../images/arrow-up.gif" width="15" height="18" hspace="0" align="left"><a href="#top">back to top</a></p>
</td>
  </tr>
  <tr> 
    <td bgcolor="#eeeeee">
<div id="footer"><a href="/index.html">home</a><!-- InstanceBeginEditable name="bottom-crumb" --> 
        &raquo; <a href="/publications.html">publications</a> &raquo; <strong>arsc 
        journal</strong><!-- InstanceEndEditable --></div>
      <div id="right">&copy;ARSC (last updated:&nbsp;</div></td>
    <td bgcolor="#eeeeee"><div id="footer-right"><!-- BeginDate --><script>
document.write(document.lastModified);
</script><!-- EndDate -->)&nbsp;&nbsp;|&nbsp;&nbsp;<a href="mailto:kimpeachpreserves@gmail.com">ARSC webmaster</a></div></td>
  </tr>
</table>
<!-- End of #footer -->
<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-5113849-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd --</html>
