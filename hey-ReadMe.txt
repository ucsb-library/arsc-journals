2014-11-14  The web interface to the ARSC journal index used to be hosted on a UCSB server but with their scheduled upgrade of that server (fms1) the XSLT based API is no longer available.  So we're converting to use the PHP based API.
That is the point of this "journal-index/" directory and its contents.
Please contact Ian Lessing or David Seubert at the UCSB Library if you have questions.
thanks, ---Ian

old link: http://fms1.library.ucsb.edu/fmi/xsl/arsc/arscjournal.xsl
