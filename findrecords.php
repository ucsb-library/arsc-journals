<?php
    /**
    * FileMaker PHP Site Assistant Generated File
    */
    require_once 'fmview.php';
    require_once 'FileMaker.php';
    require_once 'error.php';

    $cgi = new CGI();
    $cgi->storeFile();
    
    $databaseName = 'ARSC Index';
    $layoutName = 'web';

    $fm = new FileMaker();
    $fm->setProperty('database', $databaseName);    
    ExitOnError($fm);
    $layout = $fm->getLayout($layoutName);
    ExitOnError($layout);

    // formats for dates and times
    $displayDateFormat = '%m/%d/%Y';
    $displayTimeFormat = '%I:%M:%S %P';
    $displayDateTimeFormat = '%m/%d/%Y %I:%M:%S %P';
    $submitDateOrder = 'mdy';

    class EmptyRecord {
        function getRelatedSet($relationName) {
            return array(new EmptyRecord());
        }

        function getField($field, $repetition = 0) {
        }

        function getRecordId() {
        }
    }

    $record = new EmptyRecord();    
?><!DOCTYPE HTML>
<head>
<!-- InstanceBegin template="/Templates/subpage.dwt" codeOutsideHTMLIsLocked="true" -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-3LC513DQ73"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-3LC513DQ73');
</script>


<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0, user-scalable=yes" />

<link rel="stylesheet" type="text/css" media="screen" href="arsc-journal-index.css" />
<link rel="stylesheet" type="text/css" href="http://www.arsc-audio.org/arsc.css" />

<!-- InstanceBeginEditable name="doctitle" -->
<title>ARSC Journal Index(Association for Recorded Sound Collections)</title>
<!-- InstanceEndEditable -->    
<link rel="shortcut icon" href="/images/favicon.ico" type="image/vnd.microsoft.icon"> 
<link rel="icon" href="/images/favicon.ico" type="image/vnd.microsoft.icon"> 
<!-- InstanceBeginEditable name="head" -->
<meta name="description" content="The ARSC Journal is a bi-annual, peer reviewed publication that serves to document the history of sound recording and includes original articles on many aspects of research and preservation: biography; cataloging; copyright law; current research; discography; technical aspects of sound restoration, etc., etc. Selected ARSC conference papers are a regular feature. The journal also includes book, CD-ROM and sound recording reviews, and publishes a running bibliography of articles appearing in other specialist publications and of related interest. (Association for Recorded Sound Collections).  ">
<meta name="keywords" content="arsc journal bi-annual peer reveied publication arsc association recorded sound collections">
<!-- InstanceEndEditable -->

<style type="text/css">
  table {
    width:100%;
    border:0;
    border-spacing: 0px;
    }
  td {
    padding:0px;
    }
  tr.grey {
    background-color:#E5E5E5;
    }
  tr.grey2 {
    background-color:#EEEEEE;
    }	
  td.black {
    background-color:#000000;
    }
  td.grey {
    background-color:#EEEEEE;
    }
  td.wd56{
    width:56%;
   }
  td.wd44{
    width:44%;
   }
#footerb {
	margin: 0px;
	padding-top: 6px;
	padding-bottom: 6px;
	padding-left: 13px;
	font-size: 9pt;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	float: left;
}
#footerb a:link, #footerb a:visited  {
	color: #831005;
	text-decoration: none;
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #610C04;
}
#footerb a:hover   {
	color: #B51606;
	text-decoration: none;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #B51606;
}
</style>
</head>

<body>
<div id="skip-links" title="Links that allow you to skip to certain parts of the page">
	<a href="#main-menu">Skip to navigation</a>	
	<a href="#content">Skip to content</a>
	<a href="/contact.html">Contact us about accessibility issues</a>
</div>
<table>
  <tr class="grey"> 
    <td class="wd56" id="top"><img src="images/arsc.gif" alt="Association for Recorded Sound Collections (ARSC)" width="574" height="73"></td>
    <td class="wd44">      
      <form action="http://www.arsc-audio.org/search-results.html" id="cse-search-box">
        <div id="banner-menu"> 
          <input type="hidden" name="cx"  value="005131583914068578997:kwfltgaqcgg" />
          <input type="hidden" name="cof" value="FORID:9" />
          <input type="hidden" name="ie"  value="UTF-8" />
          <input type="text"   name="q"   size="20" />
          <input type="submit" name="sa"  value="Search" />
		<script type="text/javascript" src="http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en"></script>
		<br>
		<a href="/about.html">about</a> | <a href="/join.html">join or renew</a> 
           | <a href="/members-only-login.html">member login</a> 
		   | <a href="/contact.html">contact</a> 
		   <!-- Facebook Badge START -->
		   | <a href="http://www.facebook.com/pages/Association-for-Recorded-Sound-Collections-ARSC/165687321898"><img src="/images/f_logo.png" width="20" height="20" alt="FB" style="border: 0px;" /></a>
		   <!-- Facebook Badge END --> 
		   <!-- LinkedIn Badge START --> 
		   <a href="https://www.linkedin.com/groups?home=&amp;gid=3967774&amp;trk=groups_statistics-h-logo&amp;goback=%2Egmr_3967774"><img src="http://www.arsc-audio.org/images/linkedin_logo.png" width="20" height="20" alt="LI" style="border: 0px;" /></a><!-- LinkedIn Badge END --><br/>
        </div>
	  </form>
	</td>
  </tr>
  <tr> 
    <td class="black"><div id="crumbs"><a href="/index.html">home</a>
        <!-- InstanceBeginEditable name="top-crumb" --> 
        &raquo; <a href="/publications.html">publications</a> 
        &raquo; <a href="/journal.html">arsc journal</a> 
        &raquo; <strong>index</strong>
        <!-- InstanceEndEditable -->
        </div>
      <div id="pubs"><a href="/publications.html">publications</a>: <a href="/newsletter/index.html">newsletter</a>&nbsp;</div></td>
    <td class="black"><div id="pubs2">| <a href="/journal.html">journal</a> | <a href="/cds.html">conference recordings</a></div></td>
  </tr>
  <tr> 
    <td colspan="2">
		<div id="subpage-content">
        <!-- InstanceBeginEditable name="page-content" --> 
			<!-- Content -->
			<div id="content">
				<h1>ARSC Journal Index: Find Records</h1>
				<ul id="local-links">
				<?php 
					$storedLink = $cgi->get('-link');
					if (is_null($storedLink)) $cgi->store('-link', 'home'); 
					if (isset($cgi)) {
						$activelink = $cgi->get('-link');
						$cgi->clear('-link');
					}
					$localURLs =  array('find' => '<a href="findrecords.php?-link=find">Find Records</a>',
										'all' => '<a href="recordlist.php?-max=25&amp;-skip=0&amp;-link=all&amp;-action=findall&amp;-sortfieldone=Volume&amp;-sortorderone=ascend">Show all Records</a>',
										);
									  
					foreach ($localURLs as $name => $link) {
						if (isset($activelink) && (strcmp($name, $activelink) === 0)) {
							echo "<li class='activelink'><img height='10' width='11' alt='Find Records' src='./images/arrow-right.gif'>$link</li>";
							unset($activelink);
						} else {
							echo "<li><img height='10' width='11' alt='Find Records' src='./images/arrow-right.gif'>$link</li>";
						}
					}
				?>
				</ul>
        
				<form method="post" action="recordlist.php">
					<input type="hidden" name="-lay" value="<?php echo '$layoutName'?>" />
					<input type="hidden" name="-action" value="find" />
					<input type="hidden" name="-skip" value="0" />
				
					<table class="record">
						<!-- Display search field input controls -->
						<tr class="field">
							<td class="field_name"><label for="name">Keyword</label></td>
							<td class="field_data first">
								<?php $fieldName = 'Keyword';?>
								<select id="name" tabindex="1" name="<?php echo getFieldFormName($fieldName.'.op', 0, $record, false, 'EDITTEXT', 'text');?>">
									<option value="cn" selected="selected">Contains</option>
									<option value="bw">Begins With</option>
									<option value="ew">Ends With</option>
									<option value="eq">Equals</option>
									<option value="lt">Less Than</option>
									<option value="lte">Less Than or Equals</option>
									<option value="gt">Greater Than</option>
									<option value="gte">Greater Than or Equals</option>
								</select>
								<input tabindex="2" class="fieldinput" type="text" name="<?php echo getFieldFormName($fieldName, 0, $record, false, 'EDITTEXT', 'text');?>" />
							</td>
						</tr>
						<tr class="field">
							<td class="field_name"><label for="author">Author</label></td>
							<td class="field_data">
								<?php $fieldName = 'Author';?>
								<select id="author" tabindex="3" name="<?php echo getFieldFormName($fieldName.'.op', 0, $record, true, 'POPUPLIST', 'text');?>">
									<option value="cn" selected="selected">Contains</option>
									<option value="bw">Begins With</option>
									<option value="ew">Ends With</option>
									<option value="eq">Equals</option>
									<option value="lt">Less Than</option>
									<option value="lte">Less Than or Equals</option>
									<option value="gt">Greater Than</option>
									<option value="gte">Greater Than or Equals</option>
								</select> 
								<input tabindex="4" class="fieldinput" type="text" name="<?php echo getFieldFormName($fieldName, 0, $record, false, 'EDITTEXT', 'text');?>" />									
							</td>
						</tr>
						<tr class="field">
							<td class="field_name"><label for="title">Title</label></td>
							<td class="field_data">
								<?php $fieldName = 'Title';?>
								<select id="title" tabindex="5" name="<?php echo getFieldFormName($fieldName.'.op', 0, $record, true, 'EDITTEXT', 'text');?>">
									<option value="cn" selected="selected">Contains</option>
									<option value="bw">Begins With</option>
									<option value="ew">Ends With</option>
									<option value="eq">Equals</option>
									<option value="lt">Less Than</option>
									<option value="lte">Less Than or Equals</option>
									<option value="gt">Greater Than</option>
									<option value="gte">Greater Than or Equals</option>
								</select> 
								<input tabindex="6" class="fieldinput" type="text" name="<?php echo getFieldFormName($fieldName, 0, $record, true, 'EDITTEXT', 'text');?>" /> 
							</td>
						</tr>
						<tr class="field">
							<td class="field_name"><label for="volume">Volume</label></td>
							<td class="field_data">							
								<?php $fieldName = 'Volume';?>
								<select id="volume" tabindex="7" name="<?php echo getFieldFormName($fieldName.'.op', 0, $record, true, 'EDITTEXT', 'text');?>">
									<option value="cn" selected="selected">Contains</option>
									<option value="bw">Begins With</option>
									<option value="ew">Ends With</option>
									<option value="eq">Equals</option>
									<option value="lt">Less Than</option>
									<option value="lte">Less Than or Equals</option>
									<option value="gt">Greater Than</option>
									<option value="gte">Greater Than or Equals</option>
								</select> 
								<input tabindex="8" class="fieldinput" type="text" name="<?php echo getFieldFormName($fieldName, 0, $record, true, 'EDITTEXT', 'text');?>" /> 
							</td>
						</tr>
						<tr class="field">
							<td class="field_name"><label for="year">Year</label></td>
							<td class="field_data">
								<?php $fieldName = 'Year';?>
								<select id="year" tabindex="9" name="<?php echo getFieldFormName($fieldName.'.op', 0, $record, false, 'EDITTEXT', 'number');?>">
									<option value="eq">Equals</option>
									<option value="lt">Less Than</option>
									<option value="lte">Less Than or Equals</option>
									<option value="gt">Greater Than</option>
									<option value="gte">Greater Than or Equals</option>
								</select> 
								<?php $fieldValue =$record->getField('Year', 0) ; ?>
								<input tabindex="10" class="fieldinput" type="text" name="<?php echo getFieldFormName($fieldName, 0, $record, false, 'EDITTEXT', 'number');?>" /> 
							</td>
						</tr>
						<tr class="field">
							<td class="field_name"><label for="resource_type">Resource Type</label></td>
							<td class="field_data">
								<?php $fieldName = 'Resource_Type';?>
								<select id="resource_type" tabindex="11" name="<?php echo getFieldFormName($fieldName, 0, $record, false, 'EDITTEXT', 'number');?>">
									<option value="">&nbsp;</option>
									<option value="Journal Article">Journal Article</option>
									<option value="Conference Recording">Conference Recording</option>
									<option value="Conference Slides">Conference Slides</option>
								</select> 
								<?php $fieldValue =$record->getField('Resource_Type', 0) ; ?>
							</td>
						</tr>
						<!--Display search form controls-->
						<tr>
							<td></td>
							<td class="button_row">
								<input id="or" value="or" tabindex="12" type="radio" name="-lop"><label for="or">Find records if any fields match</label><br>
								<input id="and" value="and" tabindex="13" type="radio" name="-lop" checked><label for="and">Find records if all fields match</label><br>
							</td>
						</tr>
						<tr>
							<td></td>
							<td class="button_row">
								<label for="display">Display:</label>
								<select id="display" tabindex="14" name="-max">
									<option value="10">10</option>
									<option value="25" selected="selected">25</option>
									<option value="50">50</option>
									<option value="100">100</option>
									<option value="all">Max</option>
								</select> 
								Records
							</td>
						</tr>
						<tr>
							<td></td>
							<td class="button_row last">
								<input tabindex="15" type="submit" name="-find" value="Find Records" />
								<input tabindex="16" class="reset" type="reset" name="Reset" value="Reset" />
							</td>
						</tr>
					</table>
				</form>
			</div> 
			<!-- End of Content -->
			<!-- Footer-->
			<div id="footer">
				<div>
					<div id="footer-left"> </div>
					<p>&nbsp;</p>
					<p><img src="../images/arrow-up.gif" width="15" height="18" alt="Up" style="margin: 0px 0px; float=left;"><a href="#top">back to top</a></p>
				</div>
			</div>
		</div>		
	</td>
  </tr>
  <tr class="grey2"> 
    <td>
		<div id="footerb">  <!-- sosi This is a repeat div with id footer!!! -->
			<a href="/index.html">home</a>
			<!-- InstanceBeginEditable name="bottom-crumb" -->
			&raquo; <a href="/publications.html">publications</a> &raquo; <strong>arsc journal</strong>
			<!-- InstanceEndEditable -->
		</div>
		<div id="right">&copy;ARSC (last updated:&nbsp;</div>
	</td>
    <td>
		<!-- BeginDate and Webmaster Mailto -->	
		<div id="footer-right">
			<script type="text/javascript">document.write(document.lastModified);</script>)&nbsp;&nbsp;|&nbsp;&nbsp;<a href="mailto:kimpeachpreserves@gmail.com">ARSC webmaster</a>
		</div>
		<!-- EndDate -->		
	</td>
  </tr>
</table>

<!-- The script below for CSS Media ruins drop down menus in FireFox on some systems -->
<!-- script type="text/javascript" src="js/css3-mediaqueries.js" --><!-- /script -->
<script type="text/javascript">
  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
  var pageTracker = _gat._getTracker("UA-5113849-1");
  pageTracker._initData();
  pageTracker._trackPageview();
</script>
</body>
<!-- InstanceEnd -->
</html>
