# ARSCJournal
## Filemaker CWP ARSC Website Journal PHP Code
### ARSC = Association for Recorded Sound Collections

This repository contains PHP code for the Journal portion of the [ARSC Website](http://www.arsc-audio.org). The [Journal](http://www.arsc-audio.org/journal-index) portion interacts with the Library FileMaker server using its CWP (Custom Web Publishing) abilities. Typically, CWP websites that access
our FMS run on Webservers hosted in the Library. **ARSC is the exception.** The ARSC website is managed by external administrators and housed on their chosen web hosting service.

- External Contact: Nathan Georgitis <nathang@uoregon.edu>
- Internal Contacts:
  - David Seubert <seubert@library.ucsb.edu>
  - Dr. S.A. Smith <ssmith@library.ucsb.edu>
  - Ian Lessing <ilessing@library.ucsb.edu>

## Production Website Flow
As depicted in the image below, requests for the ARSC website are sent to their chosen hosting service, currently **Bluehost**. A subset of the ARSC website, ARSC Journal, accesses the Library FileMaker Server to obtain information in its ARSC Database. Such DB Access requests are sent to the Library HAproxy servers. In turn, those servers relay the request to the Library FMS on a protected private VLAN.

![Alma2NetApp](./readme/webflow.png)

## Code Updates
There are no automatic updates to ARSC Journal web pages when code is altered in the GHE repository. Updates must be performed **manually**. Such updates will usually require

1. A Git Client to clone/pull a local copy of the ARSC repository.
2. A SFTP Client to place the local ARSC code files onto the remote web host.
3. Credentials to access the remote web host.

The image below depicts such a process, one that is used by Scott on his Windows workstation. In this case GitKraken is his Git client and WinSCP is his SFTP client. GitKraken is used to clone/pull the latest code from the ARSC repository locally (onto his workstation) and WinSCp subsequently used to copy that code onto the web hosting (bluehost) used by ARSC.

![Alma2NetApp](./readme/workflow.png)

- Bluehost SFTP Access: SFTP, www.arsc-audio.org, port 22
- Bluehost Access Credentials: [Secret Server 137](https://epm.library.ucsb.edu/SecretView.aspx?secretid=137)
- Bluehost ARSC Journal Location: /home4/arscaudi/public_html/journal-index

## FileMaker Folder
This folder is/was initially copied from the FileMaker server into the ARSC repository. The files in the folder are updated from FileMaker whenever newer FMS versions are deployed and then mostly left untouched.  There is one exception, file **FileMaker/conf/filemaker-api.php**. That will be altered from what is provided by FileMaker. Therein, one MUST set the value of variable **$__FM_CONFIG['hostspec']** to the URL of the Library FileMaker server. Currently that will be **http://arsc.library.ucsb.edu**, a FQDN that points to the Library HAProxy servers. The HAproxy servers relay requests for that URL through to the Library FileMaker Server on a private internal VLAN. This largely helps with security in avoiding exposure of FileMaker to public access.
