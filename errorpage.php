<!DOCTYPE HTML>
<?php

	/**
	* FileMaker PHP Site Assistant Generated File
	*/
	global $errormessage;
	global $errorCode;
	
	$cgi = new CGI();
    $cgi->storeFile();
	
	if ($errorCode == 401) {
		$title = 'No Records Found';
		$cgi->store('-link', 'find');
	}
	else 
		$title = 'Error';
?>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<meta name="viewport" content="height=device-height,width=device-width; initial-scale=1.0; user-scalable=yes;" />
	
		<link rel="stylesheet" type="text/css" media="screen" href="arsc-journal-index.css" />
	
		<title>ARSC Journal Index | <?php print $title; ?></title>    
	</head>
	<body>
		<div id="skip-links" title="Links that allow you to skip to certain parts of the page">
			<a href="#main-menu">Skip to navigation</a>	
			<a href="accessibility">Contact us about accessibility issues</a>
		</div>         
		
		<!-- Header -->
		<div id="header">				
			<div class="logo-wrapper">
				<a id="logo" href="http://arsc-audio.org/index.html" alt="Association for Recorded Sound Collections (ARSC) Home page"></a>
			</div>
			<div id="main-menu" class="header-links">
				<a href="http://arsc-audio.org/contact.html">contact</a><span>|</span>
				<a href="http://arsc-audio.org/arsclist.html">ARSClist</a><span>|</span>
				<a href="http://arsc-audio.org/join.html">join</a><span>|</span>
				<a href="http://arsc-audio.org/about.html">about</a>
			</div>
		</div>
		<!-- End of Header -->
			
		<!-- Breadcrumbs and Navigation Menu -->
		<?php 
			$breadcrumbs = array(
				'home' => 'http://arsc-audio.org/index.html',
				'arsc journal' => 'http://arsc-audio.org/journal.html',
				'index' => './home.php',
				strtolower($title) => '',
			  );
			$pubURLs =  array('publications' => 'http://arsc-audio.org/publications.html',
							  'newsletter' => 'http://arsc-audio.org/newsletter/',
							  'journal' => 'http://arsc-audio.org/journal.html',
							  'conference recordings' => 'http://arsc-audio.org/cds.html',
							  );
		?>
		<div class="subheader">
			<div>
				<ul class="nav-links breadcrumbs">
					<?php
						$i = 0;
						$count = count($breadcrumbs);
						foreach ($breadcrumbs as $crumb => $link):
							if ($i == 0):
							  $i++;
					?>
					<li class="first"><?php echo "<a href='$link'>$crumb</a><span>»</span>"; ?></li>
					<?php 	elseif ($count != ($i + 1)):
								$i++;
					?>
					<li><?php echo "<a href='$link'>$crumb</a><span>»</span>"; ?></li>
					<?php 	else: ?>
					<li class="last"><?php echo "$crumb"; ?></li>
					<?php 	endif; ?>
					<?php endforeach; ?>
				</ul>
				
				<ul class="nav-links pubs">
					<?php
						$i = 0;
						$count = count($pubURLs);
						foreach ($pubURLs as $pub => $link):
							if ($i == 0):
							  $i++;
					?>
					<li class="first"><?php echo "<a href='$link'>$pub</a><span>:</span>"; ?></li>
					<?php 	elseif ($count != ($i + 1)):
								$i++;
					?>
					<li><?php echo "<a href='$link'>$pub</a><span>|</span>"; ?></li>
					<?php 	else: ?>
					<li class="last"><?php echo "<a href='$link'>$pub</a>"; ?></li>
					<?php 	endif; ?>
					<?php endforeach; ?>
				</ul>
			</div>
		</div>
			
		<!-- Content -->
		<div id="content">				
			<h1>ARSC Journal Index: <?php print $title; ?></h1>
			
			<ul id="local-links">
				<?php 
					$storedLink = $cgi->get('-link');
					if (is_null($storedLink)) $cgi->store('-link', 'home'); 
	
					if (isset($cgi)) {
						$activelink = $cgi->get('-link');
						$cgi->clear('-link');
					}
					
					$localURLs =  array('find' => '<a href="findrecords.php?-link=find">Find Records</a>',
										'all' => '<a href="recordlist.php?-max=25&amp;-skip=0&amp;-link=all&-action=findall">Show all Records</a>',
										);
									  
					foreach ($localURLs as $name => $link) {
						if (isset($activelink) && (strcmp($name, $activelink) === 0)) {
							echo "<li class='activelink'><img height='10' width='11' alt='Find Records' src='./images/arrow-right.gif'>$link</li>";
							unset($activelink);
						} else {
							echo "<li><img height='10' width='11' alt='Find Records' src='./images/arrow-right.gif'>$link</li>";
						}
					}
				?>
			</ul>
		
			<div id="error">
				<p><?php echo $errormessage ?></p>
			</div>
		</div> 
		<!-- End of Content -->
	
		<!-- Footer-->
		<div id="footer">
			<div>
				<div id="footer-left">
					<ul class="nav-links breadcrumbs">
						<?php
							$i = 0;
							$count = count($breadcrumbs);
							foreach ($breadcrumbs as $crumb => $link):
								if ($i == 0):
								  $i++;
						?>
						<li class="first"><?php echo "<a href='$link'>$crumb</a><span>»</span>"; ?></li>
						<?php 	elseif ($count != ($i + 1)):
									$i++;
						?>
						<li><?php echo "<a href='$link'>$crumb</a><span>»</span>"; ?></li>
						<?php 	else: ?>
						<li class="last"><?php echo "$crumb"; ?></li>
						<?php 	endif; ?>
						<?php endforeach; ?>
					</ul>
				</div>
				
				<div id="footer-right">
					<p>©ARSC (last updated:08/16/2007)</p><span>|</span>
					<a id="email" href="<?php print str_rot13('mailto:bcor@loc.gov'); ?>">ARSC webmaster</a>			
				</div>
				<script type="text/javascript">
					var e = document.getElementById('email').getAttribute('href');
					e = e.replace(/([a-zA-Z]+)\+([a-z0-9._%-]+)\+([a-z0-9._%-]+)\+([a-z.]+)/gi,'$1' + '.' + '$2' + '@' + '$3' + '.' + '$4');
					e = e.replace(/[a-zA-Z]/g,function(c) {return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);});
					document.getElementById('email').setAttribute('href', e);
				</script>
			</div>
		</div>
		<!-- End of #footer -->
		<script type="text/javascript" src="js/css3-mediaqueries.js"></script>
	</body>
</html>
